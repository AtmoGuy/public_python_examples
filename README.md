# Python Examples README

Created by: Nick Dawson aka AtmoGuy


---
## What
This repo contains various examples for standard Earth Sciences tasks. 

## Where to Start
1. Clone this repository to your local computer. 
2. Replicate the respective conda environment on your local computer. 
   * For example (Don't type any of the quotes): "conda create -n <env_name_here> -f <spec_file_name_here>"
   * For now, these tutorials will only be available on Windows 10 operating systems. Linux will be added at a later date.
3. Open Anaconda Prompt and activate the installed conda environment with "conda acitvate <env_name_here>"
4. In the Anaconda Prompt, type 'jupyter notebook' and push the Enter key
5. In the browser that pops up, navigate to the cloned directory and open the Jupyter Notebook of choice.
6. Happy Coding!
